package com.bulleratz.bradio;

public class Constants {
    public static final String STORAGE_PATH="/sdcard/bradio/";
    public static final String LOGO_STORAGE_PATH=STORAGE_PATH + "logos/";
    public static final String THEME_STORAGE_PATH=STORAGE_PATH + "themes/";
    public static final String BOUQUET_FILE=STORAGE_PATH + "bouquet";

}
