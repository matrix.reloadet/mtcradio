package com.bulleratz.bradio.mtc;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

import java.lang.reflect.Method;

public class MtcCarService {
    private static final String DESCRIPTOR = "android.microntek.ICarService";
    static final int TRANSACTION_getBooleanState = 8;
    static final int TRANSACTION_getByteArrayState = 12;
    static final int TRANSACTION_getByteState = 9;
    static final int TRANSACTION_getIntArrayState = 13;
    static final int TRANSACTION_getIntState = 10;
    static final int TRANSACTION_getParameters = 18;
    static final int TRANSACTION_getStringArrayState = 14;
    static final int TRANSACTION_getStringState = 11;
    static final int TRANSACTION_putBooleanState = 1;
    static final int TRANSACTION_putByteArraryState = 5;
    static final int TRANSACTION_putByteState = 2;
    static final int TRANSACTION_putDataChanage = 19;
    static final int TRANSACTION_putIntArraryState = 6;
    static final int TRANSACTION_putIntState = 3;
    static final int TRANSACTION_putStringArraryState = 7;
    static final int TRANSACTION_putStringState = 4;
    static final int TRANSACTION_registerCallback = 15;
    static final int TRANSACTION_setParameters = 17;
    static final int TRANSACTION_unregisterCallback = 16;


    private static MtcCarService _carService = null;

    public static synchronized MtcCarService getInstance() throws Exception {
        if (_carService == null) {
            _carService = new MtcCarService();
            if (_carService._binder == null)
                _carService = null;
        }
        return _carService;
    }

    private IBinder _binder = null;

    private MtcCarService() throws Exception {
            Class cls = Class.forName("android.os.ServiceManager");
            if (cls == null)
                throw new Exception("ServiceManager not found");
            Method method = cls.getMethod("getService", new Class[]{String.class});

            IBinder temp  = (IBinder)method.invoke(cls, new Object[]{"carservice"});
            if (temp != null) {
                _binder = temp;
            } else {
                throw new Exception("carservice not found!");
            }
    }

    public void putBooleanState(String key, boolean value) throws RemoteException {

        int i = TRANSACTION_putBooleanState;
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            if (!value) {
                i = 0;
            }
            _data.writeInt(i);
            _binder.transact(TRANSACTION_putBooleanState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putByteState(String key, byte value) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _data.writeByte(value);
            _binder.transact(TRANSACTION_putByteState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putIntState(String key, int value) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _data.writeInt(value);
            _binder.transact(TRANSACTION_putIntState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putStringState(String key, String value) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _data.writeString(value);
            _binder.transact(TRANSACTION_putStringState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putByteArraryState(String key, byte[] value) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _data.writeByteArray(value);
            _binder.transact(TRANSACTION_putByteArraryState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putIntArraryState(String key, int[] value) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _data.writeIntArray(value);
            _binder.transact(TRANSACTION_putIntArraryState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putStringArraryState(String key, String[] value) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _data.writeStringArray(value);
            _binder.transact(TRANSACTION_putStringArraryState, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public boolean getBooleanState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getBooleanState, _data, _reply, 0);
            _reply.readException();
            boolean _result = _reply.readInt() != 0;
            _reply.recycle();
            _data.recycle();
            return _result;
        } catch (Throwable th) {
            _reply.recycle();
            _data.recycle();
        }

        return false;
    }

    public byte getByteState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getByteState, _data, _reply, 0);
            _reply.readException();
            byte _result = _reply.readByte();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public int getIntState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getIntState, _data, _reply, 0);
            _reply.readException();
            int _result = _reply.readInt();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public String getStringState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getStringState, _data, _reply, 0);
            _reply.readException();
            String _result = _reply.readString();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public byte[] getByteArrayState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getByteArrayState, _data, _reply, 0);
            _reply.readException();
            byte[] _result = _reply.createByteArray();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public int[] getIntArrayState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getIntArrayState, _data, _reply, 0);
            _reply.readException();
            int[] _result = _reply.createIntArray();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public String[] getStringArrayState(String key) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(key);
            _binder.transact(TRANSACTION_getStringArrayState, _data, _reply, 0);
            _reply.readException();
            String[] _result = _reply.createStringArray();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void registerCallback(IMtcCarManageCallback callback) throws RemoteException {
        IBinder iBinder = null;
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            if (callback != null) {
                iBinder = callback.asBinder();
            }
            _data.writeStrongBinder(iBinder);
            _binder.transact(TRANSACTION_registerCallback, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void unregisterCallback(IMtcCarManageCallback callback) throws RemoteException {
        IBinder iBinder = null;
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            if (callback != null) {
                iBinder = callback.asBinder();
            }
            _data.writeStrongBinder(iBinder);
            _binder.transact(TRANSACTION_unregisterCallback, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public int setParameters(String par) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(par);
            _binder.transact(TRANSACTION_setParameters, _data, _reply, 0);
            _reply.readException();
            int _result = _reply.readInt();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public String getParameters(String par) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(par);
            _binder.transact(TRANSACTION_getParameters, _data, _reply, 0);
            _reply.readException();
            String _result = _reply.readString();
            return _result;
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }

    public void putDataChanage(String type, String state) throws RemoteException {
        Parcel _data = Parcel.obtain();
        Parcel _reply = Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeString(type);
            _data.writeString(state);
            _binder.transact(TRANSACTION_putDataChanage, _data, _reply, 0);
            _reply.readException();
        } finally {
            _reply.recycle();
            _data.recycle();
        }
    }
}
