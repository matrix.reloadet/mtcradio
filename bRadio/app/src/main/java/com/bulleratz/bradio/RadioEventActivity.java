package com.bulleratz.bradio;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class RadioEventActivity extends Activity {
    private TextView tvEventLog = null;
    private RadioService service = null;
    private Button btnClear = null;
    private Button btnRefresh = null;
    private Button btnShowRepo = null;
    private Button btnShowGain = null;
    private Button btnShowFactory = null;
    private CheckBox cbxLogRadioEvents = null;

    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            service = ((RadioServiceBinder)iBinder).getService();
            if (cbxLogRadioEvents != null) {
                cbxLogRadioEvents.setChecked(service.logRadioEventsEnabled());
            }
            updateText();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            service = null;
        }
    };

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_event_log);

        tvEventLog = findViewById(R.id.tvEventLog);
        btnClear = findViewById(R.id.btnClear);
        btnRefresh = findViewById(R.id.btnRefresh);
        btnShowRepo = findViewById(R.id.btnShowRepo);
        cbxLogRadioEvents = findViewById(R.id.cbxLogRadioEvent);
        btnShowFactory = findViewById(R.id.btnShowFactory);
        btnShowGain = findViewById(R.id.btnShowGain);

        Intent intent = new Intent(this, RadioService.class);
        bindService(intent, conn, Context.BIND_AUTO_CREATE);

        updateText();

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (service != null) {
                    service.clearRadioEvvents();
                    updateText();
                }
            }
        });

        btnShowRepo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (service != null) {
                    tvEventLog.setText(tvEventLog.getText() + "\n\n" + service.getDataRepoString());
                }
            }
        });

        cbxLogRadioEvents.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (service != null) {
                    service.setLogRadioEvent(cbxLogRadioEvents.isChecked());
                }
            }
        });
        if (service != null) {
            cbxLogRadioEvents.setChecked(service.logRadioEventsEnabled());
        }

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateText();
            }
        });

        btnShowGain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MtcFactorySettings settings = new MtcFactorySettings();
                settings.load(RadioAbstractionLayer.getAbstractionLayer(getApplicationContext()));
                tvEventLog.append("Gain: " + Integer.toString(settings.getRadioGain()) + "\nApp: " +Integer.toString(settings.getRadioApp()) + "\n");
            }
        });

        btnShowFactory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MtcFactorySettings settings = new MtcFactorySettings();
                settings.load(RadioAbstractionLayer.getAbstractionLayer(getApplicationContext()));
                tvEventLog.append(settings.toString() + "\n");
            }
        });
    }

    private void updateText() {
        if (tvEventLog != null && service != null)
            tvEventLog.setText(service.getRadioEvents());
    }

    @Override
    protected void onDestroy() {
        unbindService(conn);
        super.onDestroy();
    }
}
