package com.bulleratz.bradio;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by maucher on 06.09.2017.
 */

public class RadioService extends Service {

    private final class AutoSeekInfo {
        public RadioStation currentStation = null;
        private RadioStationBouquet _autoSeekBouquet = null;

        public void autoSeekStaarted() {
            _autoSeekBouquet = new RadioStationBouquet();
        }

        public void seekFound() {
            currentStation = new RadioStation();
            if (_autoSeekBouquet != null) {
                _autoSeekBouquet.add(currentStation);
            }
            currentStation.setFrequency(_frequency);
        }

        public void autoSeekFinished() {
            currentStation = null;
        }
    }

    AutoSeekInfo _autoSeekInfo = new AutoSeekInfo();

    public static final int BAND_FM = 0;
    public static final int BAND_AM = 1;
    public static final int FREQ_MIN_FM = 0;
    public static final int FREQ_MAX_FM = 1;
    public static final int FREQ_MIN_AM = 0;
    public static final int FREQ_MAX_AM = 1;
    public static final int FREQ_STEP_FM = 2;
    public static final int FREQ_STEP_AM = 2;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return _binder;
    }

    private RadioAbstractionLayer _radio = null;
    private int _frequency = 101800000;
    private String _psn = "";
    private String _title = "";
    private String _prevTitle = "";
    private boolean _started = false;
    private int _pty = 0;
    private int _pi = 0;
    private int _strength = 0;
    private boolean _stereo = false;
    private boolean _seeking = false;
    private boolean _autoSeeking = false;
    private boolean _tp = false;
    private boolean _ta = false;
    private boolean _continueAfterTa = false;
    private static char[] _rdsG0 = null;
    private boolean _useRdsFix = true;
    private boolean _skipTa = false;

    private final int _taOffDelay = 2000;
    private final int MSG_TA_OFF = 1;
    private final int _taSkipTime = 120000; //2mins
    private final int MSG_TA_SKIP = 2;
    private final int MSG_POW_ON = 3;
    private final int _powOnDelay = 1000;

    private boolean _taEnabled = false;
    private RdsRepository _rdsRepo = new RdsRepository();

    static BouquetManager _bouqetManager = null;
    static RadioServiceSettings _settings = null;

    private MediaSession _mediaSession = null;
    private MediaSession.Callback _mediaSessionCallback = new MediaSession.Callback() {
        @Override
        public boolean onMediaButtonEvent(Intent mediaButtonIntent) {
            // Only process key after debounce time - else it may be handled twice in MediaButtonEvent and Keydown
            if (!debounceKey())
                return true;
            try {
                if (mediaButtonIntent.getAction().equals(Intent.ACTION_MEDIA_BUTTON)) {
                    KeyEvent event = (KeyEvent) mediaButtonIntent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                    if (event != null) {
                        int action = event.getAction();
                        int code = event.getKeyCode();
                        if (action == KeyEvent.ACTION_DOWN && code == KeyEvent.KEYCODE_MEDIA_NEXT && isRadioOn()) {
                            getBouquetManager().next();
                            return true;
                        } else if (action == KeyEvent.ACTION_DOWN && code == KeyEvent.KEYCODE_MEDIA_PREVIOUS && isRadioOn()) {
                            getBouquetManager().prev();
                            return true;
                        } else if (action == KeyEvent.ACTION_DOWN && code == KeyEvent.KEYCODE_MEDIA_PAUSE) {
                            toggleRadioOn();
                            return true;
                        } else if (action == KeyEvent.ACTION_DOWN && code == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                            toggleRadioOn();
                            return true;
                        } else if (action == KeyEvent.ACTION_DOWN && code == KeyEvent.KEYCODE_MEDIA_STOP) {
                            setRadioOn(false);
                            return true;
                        } else if (action == KeyEvent.ACTION_DOWN && code == KeyEvent.KEYCODE_MEDIA_PLAY) {
                            setRadioOn(true);
                            return true;
                        }

                    }
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
            return true; /* Else other MediaSession may handle and starts Playback */
        }
    };

    private RadioStation _currentRadioStation = null;

    private IRadioStationListener _radioStationListener = new IRadioStationListener() {

        @Override
        public void radioStationChanged(RadioStation sender) {

            if (_mediaSession != null) {
                MediaMetadata.Builder meta = new MediaMetadata.Builder();
                if (sender != null) {
                    meta.putBitmap(MediaMetadata.METADATA_KEY_ART, sender.getBitmap());
                    meta.putString(MediaMetadata.METADATA_KEY_ARTIST, sender.getDisplayName());
                    meta.putString(MediaMetadata.METADATA_KEY_TITLE, sender.getCurrentTitle());
                }
                _mediaSession.setMetadata(meta.build());
            }
        }
    };

    private BouquetManager.IBouquetManagerListener _bouquetManagerListener = new BouquetManager.IBouquetManagerListener() {
        @Override
        public void selectedBouquetChanged(RadioStationBouquet bouquet) {

        }

        @Override
        public void selectedStationChanged(RadioStation station, int stationIndex) {
            if (_currentRadioStation != null)
                _currentRadioStation.removeRadioStationListener(_radioStationListener);
            _currentRadioStation = station;
            if (station != null) {
                station.addRadioStationListener(_radioStationListener);
            }
            _radioStationListener.radioStationChanged(station);
        }

        @Override
        public void bouquetAdded(RadioStationBouquet bouquet) {

        }

        @Override
        public void bouquetRemoved(RadioStationBouquet bouquet) {

        }
    };

    public static synchronized BouquetManager getBouquetManager() {
        if (_bouqetManager == null) {
            _bouqetManager = new BouquetManager();
            _bouqetManager.loadBouquets();

        }
        return _bouqetManager;
    }

    public RadioServiceSettings getSharedSettings() {
        if (_settings == null)
            _settings = new RadioServiceSettings(getBaseContext());
        return _settings;
    }

    private Handler _radioHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if ("Radio".equals((String) msg.obj)) {
                radioTask(msg.getData());
            } else if ("KeyDown".equals((String) msg.obj)) {
                keyTask(msg.getData());
            }
        }
    };

    private Handler _handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_TA_OFF) {
                if (!_continueAfterTa && !_ta)
                    setRadioOn(false);
                _skipTa = false;
            }
            if (msg.what == MSG_TA_SKIP) {
                setTaEnabled(true);
                _skipTa = false;
            }
            if (msg.what == MSG_POW_ON) {
                setRadioOnPrivate(true);
            }
        }
    };

    private static String convertRds(byte[] bytes) {
        if (_rdsG0 == null) {
            _rdsG0 = new char[255];
            for (int i = 0; i < 128; i++) {
                _rdsG0[i] = (char) i;
            }
            char _rdsG0High[] = new char[]{'á', 'à', 'é', 'è', 'í', 'ì', 'ó', 'ò', 'ú', 'ù', 'Ñ', 'Ç', 'Ş', 'ß', '¡', 'Ĳ',
                    'â', 'ä', 'ê', 'ë', 'î', 'ï', 'ô', 'ö', 'û', 'ü', 'ñ', 'ç', 'ş', 'ğ', 'ı', 'ĳ',
                    'a' /*hoch*/, 'α', '©', '‰', 'Ğ', 'ě', 'ň', 'ő', 'π', '€', '£', '$', '←', '↑', '→', '↓',
                    'o' /*hoch*/, '1' /*HOCH*/, '²', '³', '±', 'İ', 'ń', 'ü' /*Striche!*/, 'µ', '¿', '÷', '°', '¼', '½', '¾', '§',
                    'Á', 'À', 'É', 'È', 'Í', 'Ì', 'Ó', 'Ò', 'Ú', 'Ù', 'Ř', 'Č', 'Š', 'Ž', 'Đ', 'Ŀ',
                    'Â', 'Ä', 'Ê', 'Ë', 'Î', 'Ï', 'Ô', 'Ö', 'Û', 'Ü', 'ř', 'č', 'š', 'ž', 'đ', 'ŀ',
                    'Ã', 'Å', 'Æ', 'Œ', 'ŷ', 'Ý', 'Õ', 'Ø', 'þ', 'Ŋ', 'Ŕ', 'Ć', 'Ś', 'Ź', 'Ŧ', 'ð',
                    'ã', 'å', 'æ', 'œ', 'ŵ', 'ý', 'õ', 'ø', 'þ', 'ŋ', 'ŕ', 'ć', 'ś', 'ź', 'ŧ'};

            for (int i = 128; i < 128 + _rdsG0High.length; i++) {
                _rdsG0[i] = _rdsG0High[i - 128];
            }

        }


        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            if (bytes[i] < _rdsG0.length) {
                int index = (bytes[i] & 0xff);
                builder.append(_rdsG0[index]);
            } else {
                builder.append(' ');
            }
        }

        return builder.toString();
    }

    private ArrayList<IRadioServiceListener> _serviceListeners = new ArrayList<IRadioServiceListener>();

    boolean _hasAudioFocus = false;
    private int[][] freqInfo = new int[][]{new int[]{87500000, 108000000, 50000}, new int[]{522000, 1620000, 9000}};

    public void setLocEnabled(boolean enabled) {
        String enableLoc = "ctl_radio_loc=";
        if (enabled)
            enableLoc = enableLoc + Integer.toString(1);
        else
            enableLoc = enableLoc + Integer.toString(0);
        setParameters(enableLoc);
    }

    public void setAfEnabled(boolean enabled) {
        String enableAf = "ctl_radio_af=";
        if (enabled)
            enableAf = enableAf + Integer.toString(1);
        else
            enableAf = enableAf + Integer.toString(0);
        setParameters(enableAf);
    }

    private boolean _stereoEnabled = true;

    public void setStereoEnabled(boolean enabled) {
        _stereoEnabled = enabled;
        String enableSt = "ctl_radio_st="; // setParameters("ctl_radio_stereo=" PX3
        if (_stereoEnabled)
            enableSt = enableSt + Integer.toString(1);
        else
            enableSt = enableSt + Integer.toString(0);
        setParameters(enableSt);

        notifyStereo();
    }

    private void notifyStereo() {
        for (IRadioServiceListener listener : _serviceListeners)
            listener.stereoChanged(isStereo());
    }

    public void setTaEnabled(boolean enabled) {
        String enableTa = "ctl_radio_ta=";
        _skipTa = false;
        _handler.removeMessages(MSG_TA_SKIP);
        if (enabled)
            enableTa = enableTa + Integer.toString(1);
        else
            enableTa = enableTa + Integer.toString(0);
        _taEnabled = enabled;
        setParameters(enableTa);

    }

    public void setBandFm() {
        if (!isFm()) {
            setFrequency(freqInfo[BAND_FM][FREQ_MIN_FM]);
            //TODO: Debug
            int temp = freqInfo[BAND_FM][FREQ_MIN_FM];
            setFrequencyPrivate(temp);
        }
    }

    public void setBandAm() {
        if (!isAm()) {

            setFrequency(freqInfo[BAND_AM][FREQ_MIN_AM]);

            //TODO: Debug
            int temp = freqInfo[BAND_AM][FREQ_MIN_AM];
            setFrequencyPrivate(temp);
        }
    }

    public boolean getTaEnabled() {
        return _taEnabled;
    }

    public int getPty() {
        return _pty;
    }

    public String getPsn() {
        return _psn;
    }

    public String getTitle() {
        return _title;
    }

    public int getPi() {
        return _pi;
    }

    public int getStrength() {
        return _strength;
    }

    public boolean isStereo() {
        return _stereo && _stereoEnabled;
    }

    public boolean isAutoSeeking() {
        return _autoSeeking;
    }

    public boolean isSeeking() {
        return _seeking;
    }

    public boolean getTp() {
        return _tp;
    }

    public boolean getTa() {
        return _ta;
    }

    public boolean isRadioOn() {
        return getParameters("av_channel=").equals("fm");
    }

    public boolean isStereoEnabled() {
        return _stereoEnabled;
    }

    RadioServiceBinder _binder = new RadioServiceBinder() {

        @Override
        public RadioService getService() {
            return RadioService.this;
        }
    };

    private AudioManager.OnAudioFocusChangeListener _audoFocusListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int i) {
            switch (i) {
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                case AudioManager.AUDIOFOCUS_GAIN:
                    _hasAudioFocus = true;
                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    // Else the focus might be geven back
                    ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
                        @Override
                        public void onAudioFocusChange(int i) {
                        }
                    });
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    // TODO: Hier TA bis auf weiteres unterbinden??
                    _hasAudioFocus = false;
                    break;
                default:
                    break;
            }
            // Verzogert, sonst kann sync mit wegen tock app verloren gehen
            if (_hasAudioFocus) {
                _handler.sendEmptyMessageDelayed(MSG_POW_ON, _powOnDelay);
            } else {
                setRadioOnPrivate(_hasAudioFocus);
            }
        }
    };

    public RadioService() {
        getBouquetManager().setRadioService(this);
        getBouquetManager().addBouquetManagerListener(_bouquetManagerListener);
    }

    public void increaseFreq() {
        int freq = _frequency;
        int band = BAND_FM;
        if (isAm())
            band = BAND_AM;
        freq = freq + freqInfo[band][2];
        if (freq > freqInfo[band][1])
            freq = freqInfo[band][0];
        setFrequency(freq);
    }

    public void decreaseFreq() {
        int freq = _frequency;
        int band = BAND_FM;
        if (isAm())
            band = BAND_AM;
        freq = freq - freqInfo[band][2];
        if (freq < freqInfo[band][0])
            freq = freqInfo[band][1];
        setFrequency(freq);
    }

    public String getDataRepoString() {
        return _rdsRepo.toString();
    }

    private void setFrequencyRange() {
        int area = 0;
        try {
            area = Integer.parseInt(getParameters("cfg_radio_area="));
        } catch (Exception e) {
            area = 0;
        }

        switch (area) {
            default:
            case 0:
                this.freqInfo[0][0] = 87500000;
                this.freqInfo[0][1] = 108000000;
                this.freqInfo[0][2] = 100000;
                this.freqInfo[1][0] = 522000;
                this.freqInfo[1][1] = 1620000;
                this.freqInfo[1][2] = 9000;
                break;
            case 1:
                this.freqInfo[0][0] = 87500000;
                this.freqInfo[0][1] = 108000000;
                this.freqInfo[0][2] = 50000;
                this.freqInfo[1][0] = 522000;
                this.freqInfo[1][1] = 1620000;
                this.freqInfo[1][2] = 9000;
                break;
            case 2:
                this.freqInfo[0][0] = 65000000;
                this.freqInfo[0][1] = 74000000;
                this.freqInfo[0][2] = 30000;
                this.freqInfo[1][0] = 522000;
                this.freqInfo[1][1] = 1620000;
                this.freqInfo[1][2] = 9000;
                break;
            case 3:
                this.freqInfo[0][0] = 65000000;
                this.freqInfo[0][1] = 108000000;
                this.freqInfo[0][2] = 30000;
                this.freqInfo[1][0] = 522000;
                this.freqInfo[1][1] = 1620000;
                this.freqInfo[1][2] = 9000;
                break;
            case 4:
                this.freqInfo[0][0] = 87500000;
                this.freqInfo[0][1] = 108000000;
                this.freqInfo[0][2] = 100000;
                this.freqInfo[1][0] = 530000;
                this.freqInfo[1][1] = 1710000;
                this.freqInfo[1][2] = 10000;
                break;
            case 5:
                this.freqInfo[0][0] = 87500000;
                this.freqInfo[0][1] = 107900000;
                this.freqInfo[0][2] = 200000;
                this.freqInfo[1][0] = 530000;
                this.freqInfo[1][1] = 1710000;
                this.freqInfo[1][2] = 10000;
                break;
            case 6:
                this.freqInfo[0][0] = 76000000;
                this.freqInfo[0][1] = 90000000;
                this.freqInfo[0][2] = 100000;
                this.freqInfo[1][0] = 522000;
                this.freqInfo[1][1] = 1629000;
                this.freqInfo[1][2] = 9000;
                break;
            case 7:
                this.freqInfo[0][0] = 87500000;
                this.freqInfo[0][1] = 108000000;
                this.freqInfo[0][2] = 100000;
                this.freqInfo[1][0] = 522000;
                this.freqInfo[1][1] = 1710000;
                this.freqInfo[1][2] = 9000;
                break;
        }
    }

    public int getMinFrequency() {

        if (isFm())
            return freqInfo[BAND_FM][FREQ_MIN_FM];
        else
            return freqInfo[BAND_AM][FREQ_MIN_AM];
    }

    public int getMaxFrequency() {

        if (isFm())
            return freqInfo[BAND_FM][FREQ_MAX_FM];
        else
            return freqInfo[BAND_AM][FREQ_MAX_AM];
    }

    public int getFrequencyStep() {
        if (isFm())
            return freqInfo[BAND_FM][FREQ_STEP_FM];
        else
            return freqInfo[BAND_AM][FREQ_STEP_AM];
    }

    public boolean isAm() {
        return !isFm();
    }

    public boolean isFm() {
        return _frequency >= freqInfo[BAND_FM][FREQ_MIN_FM];
    }


    @Override
    public void onCreate() {
        super.onCreate();
        if (_radio == null) {
            _radio = RadioAbstractionLayer.getAbstractionLayer(this);
            _radio.attach(_radioHandler, "Radio,KeyDown");
        }


        Log.d("BRadioService", "onCreate");
        _hasAudioFocus = true;
        setRadioOn(false);

        setFrequencyRange();

        if (!_started) {
            _started = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("com.microntek.finish");
            intentFilter.addAction("com.microntek.bootcheck");
            registerReceiver(_microntecReceiver, intentFilter);
        }
    }

    BroadcastReceiver _microntecReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("com.microntek.finish") && intent.getStringExtra("class").equals("com.microntek.radio")) {
                Log.d("BRadioService", "finish received");
                setRadioOn(false);
            }
            if (action.equals("com.microntek.bootcheck")) {
                if (intent.hasExtra("class")) {
                    String klass = intent.getStringExtra("class");
                    if (klass.equals("poweroff")) {
                        setRadioOn(false);
                    }
                }
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("BRadioService", "OnStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (_radio != null)
            _radio.detach();
        Log.d("BRadioService", "onDestroy");
        unregisterReceiver(_microntecReceiver);
        setRadioOn(false);
        if (_radio != null)
            _radio.detach();
        super.onDestroy();
    }

    public void addRadioServiceListener(IRadioServiceListener listener) {
        if (!_serviceListeners.contains(listener))
            _serviceListeners.add(listener);
    }

    public void removeRadioServiceListener(IRadioServiceListener listener) {
        while (_serviceListeners.contains(listener))
            _serviceListeners.remove(listener);
    }

    private void setParameters(String para) {
        if (_radio != null)
            _radio.setParameters(para);
    }

    private String getParameters(String para) {
        if (_radio != null)
            return _radio.getParameters(para);
        return "";
        //return ((AudioManager) getSystemService(AUDIO_SERVICE)).getParameters(para);
    }

    public void setFrequency(int frequency) {
        if (frequency != _frequency) {
            setParameters("ctl_radio_frequency=" + (frequency / 1000));
        }
    }

    public int getFrequency() {
        return _frequency;
    }

    public void setRadioOn(boolean on) {
        setRadioOn(on, false);
    }

    private void setRadioOn(boolean on, boolean ta) {
        if (on && !_hasAudioFocus) {
            if (ta) {
                _hasAudioFocus = (((AudioManager) getSystemService(AUDIO_SERVICE)).requestAudioFocus(_audoFocusListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED);
                _continueAfterTa = false;
            } else {
                _hasAudioFocus = (((AudioManager) getSystemService(AUDIO_SERVICE)).requestAudioFocus(_audoFocusListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED);
                on = _hasAudioFocus;
                _continueAfterTa = true;
            }
        } else if (!on) {
            //_hasAudioFocus = !(((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(_audoFocusListener) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED);
            ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(_audoFocusListener);
            _hasAudioFocus = false; // Annahme dass es immer klappt
        }
        if (on)
            _handler.sendEmptyMessageDelayed(MSG_POW_ON, _powOnDelay);
        else
            setRadioOnPrivate(on);
    }

    public void toggleRadioOn() {
        setRadioOn(!getRadioOn());
    }

    private void setRadioOnPrivate(boolean on) {
        Log.d("BRadioService", "Set Radio On: " + Boolean.toString(on));
        if (on) {
            // Can Display
            Intent it1 = new Intent("com.microntek.canbusdisplay");
            it1.putExtra("type", "radio-on");
            sendBroadcast(it1);
            //PX5
            setParameters("av_focus_gain=fm");
            setParameters("av_channel_enter=fm");
            setParameters("ctl_radio_mute=false"); //PX3
            // Create Session
            createMediaSession();

        } else {
            // Can Display
            Intent it1 = new Intent("com.microntek.canbusdisplay");
            it1.putExtra("type", "radio-off");
            sendBroadcast(it1);
            //PX5
            setParameters("av_focus_loss=fm");
            setParameters("av_channel_exit=fm");
            setParameters("ctl_radio_mute=true"); //PX3
            _hasAudioFocus = false;
            releaseMediaSession();
        }
    }

    private void createMediaSession() {
        if (_mediaSession != null) {
            releaseMediaSession();
        }
        if (_mediaSession == null) {
            _mediaSession = new MediaSession(this, "GMRADIO");
            _mediaSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
            _mediaSession.setCallback(_mediaSessionCallback);
            _mediaSession.setActive(true);

            PlaybackState.Builder builder = new PlaybackState.Builder();
            builder.setState(PlaybackState.STATE_PLAYING, 0, 1.0f);
            _mediaSession.setPlaybackState(builder.build());

            /* Workaround: Since Oreo 8.0, MediaKeys are routed to the last active Session that played sound??? */
            final MediaPlayer mMediaPlayer;
            mMediaPlayer = MediaPlayer.create(this, R.raw.silent);
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mMediaPlayer.release();
                }
            });
            mMediaPlayer.start();


        }
    }

    private void releaseMediaSession() {
        if (_mediaSession != null) {
            PlaybackState.Builder builder = new PlaybackState.Builder();
            builder.setState(PlaybackState.STATE_PAUSED, 0, 1.0f);
            _mediaSession.setPlaybackState(builder.build());
            _mediaSession.setActive(false);
            _mediaSession.release();
            _mediaSession = null;
        }
    }

    private String _prevPsn = "";

    private void setPsn(String psn) {
        /*if (psn.equals(_prevPsn) || _psn.isEmpty() || psn.isEmpty())*/
        {
            //TODO: llerstring
            /*if (!_psn.equals(psn))*/
            {

                _psn = psn;
                for (IRadioServiceListener listener : _serviceListeners)
                    listener.psnAvailable(_psn);
            }
            //_radioEventLog += "\nSet PSN: " + _psn.toString() + "\n";
        }
        _prevPsn = psn;
        _scanStatus.updateStation();
    }

    private void setTitle(String title) {
        if (title.equals(_prevTitle) || title.isEmpty() || _prevTitle.isEmpty()) {
            _title = title;
            for (IRadioServiceListener listener : _serviceListeners)
                listener.titleAvailable(_title);
            //_radioEventLog += "\nSet Title: " + _title.toString() + "\n";
        }
        _prevTitle = title;
        /*if (!_title.equals(title))
        {
            _title = title;
            for (IRadioServiceListener listener : _serviceListeners)
                listener.titleAvailable(_title);
        }*/
    }

    private void setFrequencyPrivate(int frequency) {
        _scanStatus.updateFrequency(frequency);
        if (frequency != _frequency) {
            boolean isFm = isFm();
            _frequency = frequency;
            boolean bandChanged = (isFm == isAm());
            for (IRadioServiceListener listener : _serviceListeners) {
                listener.frequencyChanged(_frequency);
                if (bandChanged)
                    listener.bandChanged();
            }

            RdsRepository.RdsData rdsData = _rdsRepo.getDataByFreq(frequency);
            if (_seeking || _autoSeeking) {
                rdsData.title = "";
                rdsData.name = "";
            }

            /* Force Update */
            _prevPsn = rdsData.name;
            _prevTitle = rdsData.title;

            setPsn(rdsData.name);
            setTitle(rdsData.title);

            _pi = rdsData.pi;
            _pty = 0;
            _strength = 0;
            _ta = false;
            _tp = false;
            _stereo = false;

            for (IRadioServiceListener listener : _serviceListeners) {
                listener.piChanged(_pi);
                listener.ptyChanged(_pty);
                listener.strengthChanged(0);
                listener.taChanged(false);
                listener.tpChanged(false);
                listener.stereoChanged(false);
            }
        }

        // CAN Display
        Intent it1 = new Intent("com.microntek.canbusdisplay");
        it1.putExtra("type", "radio");
        //int freCount = this.freq.length;
        //it1.putExtra("group", (freCount << 4) | this.mBand);
        //if (this.mBand < freCount - 2) {
        //    it1.putExtra("fre", this.mFreq / 10000);
        //} else {
        //    it1.putExtra("fre", this.mFreq / 1000);
        //}
        //it1.putExtra("index", (byte) this.mChannel);

        if (isFm()) {
            it1.putExtra("group", (7 << 4)); //Bitmask: Num Bands -> 111 000 <- Band index (Upper Two AM, rest FM)
            it1.putExtra("fre", this._frequency / 10000);
        } else {
            it1.putExtra("group", (7 << 4) | 5);
            it1.putExtra("fre", this._frequency / 1000);
        }
        it1.putExtra("index", (byte) getBouquetManager().getSelectedStationIndex());
        getApplicationContext().sendBroadcast(it1);
    }

    private boolean getRadioOn() {
        return getParameters("av_channel=").equals("fm");
    }

    public void seekNext() {
        setParameters("ctl_radio_seek=up");
    }

    public void seekPrev() {
        setParameters("ctl_radio_seek=down");
    }

    public void tuneUp() {
        setParameters("ctl_radio_tune=up");
    }

    public void tuneDown() {
        setParameters("ctl_radio_tune=down");
    }

    public void seekAuto() {
        setParameters("ctl_radio_seek=auto");
    }

    long _lastKeyPressTime = 0;
    long _keyPressDebounceTime = 200;

    private boolean debounceKey() {
        if ((System.currentTimeMillis() - _lastKeyPressTime) >= _keyPressDebounceTime) {
            _lastKeyPressTime = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    private void keyTask(Bundle bundle) {
        // Only process key after debounce time - else it may be handled twice in MediaButtonEvent and Keydown
        if (!debounceKey() || !isRadioOn())
            return;

        if (!getSharedSettings().getUseMicrontekKeys())
            return;
        if ("key".equals(bundle.getString("type"))) {
            int keyCode = (bundle.getInt("value"));
            switch (keyCode) {
                case 260:
                case 299:
                    getBouquetManager().prev();
                    break;
                case 268:
                case 300:
                    getBouquetManager().next();
                    break;
                case 276:
                    seekPrev();
                    break;
                case 278:
                    seekNext();
                    break;
                case 311:
                    tuneDown();
                    break;
                case 312:
                    tuneUp();
                    break;
            }
        }
    }

    public ScanStatus getAutoscanManager() {
        return _scanStatus;
    }

    public class ScanStatus {
        RadioStationBouquet bouquet = null;
        boolean scanning = false;
        boolean running = false;
        RadioStation currentStation = null;
        int startFreq = 0;
        int endFreq = 0;
        int stepWidth = 0;
        ArrayList<AutoScanObserver> _scanObservers = new ArrayList<>();

        public void addObserver(AutoScanObserver observer) {
            if (observer != null && !_scanObservers.contains(observer)) {
                _scanObservers.add(observer);
            }
        }

        public void removeObserver(AutoScanObserver observer) {
            if (observer != null) {
                while (_scanObservers.contains(observer)) {
                    _scanObservers.remove(observer);
                }
            }
        }

        private Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                seekNext();
            }
        };

        private void waitToSeek() {
            Message msg = handler.obtainMessage(0);
            handler.removeMessages(0);
            handler.sendMessageDelayed(msg, 2100);
        }

        private void seekNext() {
            handler.removeMessages(0);
            RadioService.this.seekNext();
        }

        public void addStation(int freq, int strength) {
            if (scanning && running && strength >= Settings.getAutoScanThreshold()) {
                currentStation = new RadioStation();
                updateStation();
                bouquet.add(currentStation);
                for (AutoScanObserver observer : _scanObservers) {
                    observer.stationFound(currentStation);
                }
                waitToSeek();
            }
        }

        public void updateStation() {
            if (scanning && running && currentStation != null) {
                currentStation.setFrequency(_frequency);
                currentStation.setPi(_pi);
                currentStation.setName(_psn);
                if (currentStation.getFrequency() != 0 &&
                currentStation.getPi() != 0 &&
                !currentStation.getName().isEmpty()) {
                    seekNext();
                }
            }
        }

        public void cancel() {
            scanning = false;
            running = false;
            setParameters("ctl_radio_frequency=" + (_frequency / 1000));
            for (AutoScanObserver observer : _scanObservers) {
                observer.scanFinished();
            }
        }

        public RadioStationBouquet start(int band) {
            handler.removeMessages(0);
            if (scanning) {
                cancel();
            }
            scanning = true;
            running = false;
            bouquet = new RadioStationBouquet();
            bouquet.setName("Autoscan");
            startFreq = freqInfo[band][FREQ_MIN_FM];
            endFreq = freqInfo[band][FREQ_MAX_FM];
            stepWidth = freqInfo[band][FREQ_STEP_FM];

            setFrequency(endFreq);
            for (AutoScanObserver observer : _scanObservers) {
                observer.scanStarted(bouquet);
            }
            return bouquet;
        }

        public void updateFrequency(int frequency) {
            if (frequency == endFreq && scanning && !running) {
                seekNext();
                return;
            }


            if (frequency == startFreq && scanning) {
                if (running) {
                    /* TODO: Cancel with delay, falls seek found? */
                    cancel();
                    return;
                }
                running = true;
            }
            for (AutoScanObserver observer : _scanObservers) {
                observer.progress(startFreq, endFreq, frequency);
            }
        }

        public void seekFound(int freq, int strength) {
            addStation(freq, strength);
        }

        public boolean isScanning() {
            return scanning;
        }
    }

    ScanStatus _scanStatus = new ScanStatus();

    public static String bundleToString(Bundle bundle) {
        if (bundle == null) {
            return "";
        }
        String ret = "";
        Object[] keys = bundle.keySet().toArray();

        for (Object k : keys) {
            String key = k.toString();
            ret += "Key: " + key.toString() + "\n";
            Object value = bundle.get(key);
            ret += "Value: " + value.toString()+ " ";
            Class klass = value.getClass();
            ret += "(" + klass.getName() + ")\n";
        }
        ret += "\n";
        return ret;
    }

    private String _radioEventLog = "";
    private boolean _logRadioEvents = false;

    private void logRadioEvent(Bundle bundle) {
        String type = bundle.getString("type");
        if (type != null && type.equals("pi"))
            return;
        if (_logRadioEvents) {
            _radioEventLog += new SimpleDateFormat("HH:mm:ss.SSS").format(new Date()) + "\n";
            _radioEventLog += bundleToString(bundle);
        }
    }

    public void clearRadioEvvents() {
        _radioEventLog = "";
    }

    public String getRadioEvents() {
        return  _radioEventLog;
    }

    public boolean logRadioEventsEnabled() {
        return _logRadioEvents;
    }

    public void setLogRadioEvent(boolean enable) {
        _logRadioEvents = enable;
    }


    private void radioTask(Bundle bundle) {
        String type = bundle.getString("type");
        if (logRadioEventsEnabled()) {
            logRadioEvent(bundle);
        }

        //Freuqency
        if (type.equals("freq")) {

            int newFreq = bundle.getInt("value", 87500000);
            if (newFreq != _frequency) {
                setFrequencyPrivate(newFreq);
            }
        }
        //Title
        else if (type.equals("rt")) {
            String title = "";
            byte[] b = bundle.getByteArray("value");
            if (_useRdsFix) {
                title = convertRds(b);
            } else {
                try {
                    title = new String(b, "iso8859-1").trim();
                } catch (UnsupportedEncodingException e2) {
                    //TODO: don't set???
                }
            }

            setTitle(title.trim());
            _rdsRepo.update(_frequency, _pi, _psn, title.trim());
        }
        //Station name
        else if (type.equals("psn")) {
            //if (RadioService.this.radioCheckValid && RadioService.this.rdsUI) {
            byte[] b = bundle.getByteArray("value");
            String psn = "";
            if (_useRdsFix) {
                psn = convertRds(b);
            } else {
                try {
                    psn = new String(b, "iso8859-1").trim();
                } catch (UnsupportedEncodingException e2) {
                    //TODO: Do not set on error
                }
            }
            setPsn(psn);
            _rdsRepo.update(_frequency, _pi, psn, _title);
        }
        // Program Type
        else if (type.equals("pty")) {
            int pty = bundle.getInt("value", 0);
            if (_pty != pty) {
                _pty = pty;
                for (IRadioServiceListener listener : _serviceListeners)
                    listener.ptyChanged(_pty);
            }
        }
        // PI
        else if (type.equals("pi")) {
            int pi = bundle.getInt("value", 0);
            _scanStatus.updateStation();
            if (_pi != pi) {
                _pi = pi;
                RdsRepository.RdsData data = _rdsRepo.getDataByPu(pi);
                if (data.title != null && !data.title.isEmpty()) {
                    _prevTitle = data.title; // Force Update
                    setTitle(data.title);
                }
                if (data.name != null && !data.name.isEmpty()) {
                    _prevPsn = data.name; // Force Update
                    setPsn(data.name);
                }
                for (IRadioServiceListener listener : _serviceListeners)
                    listener.piChanged(_pi);
            }
        }
        // Stereo
        else if (type.equals("stereo")) {
            boolean st = bundle.getInt("value", 0) == 1;
            boolean oldStereo = isStereo();
            _stereo = st;
            if (oldStereo != isStereo()) {
                notifyStereo();
            }

        }
        // TP
        else if (type.equals("tp")) {
            boolean tp = bundle.getInt("value", 0) == 1;
            if (tp != _tp) {
                _tp = tp;
                for (IRadioServiceListener listener : _serviceListeners)
                    listener.tpChanged(_tp);
            }
        }
        // TA
        else if (type.equals("ta")) {
            boolean ta = bundle.getInt("value", 0) == 1;
            if (ta != _ta) {
                _ta = ta;
                for (IRadioServiceListener listener : _serviceListeners)
                    listener.taChanged(_ta);
            }
            if (_ta && !isRadioOn()  && !_skipTa) {
                // TODO: Request usw.
                setRadioOn(true, true);
                Intent intent = new Intent(this, TrafficMessageDialog.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (!_ta){
                // If the Radio is turned on after receiveing TA, it received a TA off and turns off again.
                // Therefore, turn off after a delay (because we may receive a TA on again)
                if (!_continueAfterTa)
                    _handler.sendEmptyMessageDelayed(MSG_TA_OFF, _taOffDelay);
            }
        }
        //Strength - nur bei seek found???
        /*else if (type.equals("strength")) {
            int strength = bundle.getInt("strength", 0);
            if (strength != _strength) {
                _strength = strength;
                for (IRadioServiceListener listener : _serviceListeners)
                    listener.strengthChanged(_strength);
            }
        }*/
        // Seek start
        else if (type.equals("seek_start"))
        {
            _seeking = true;
            for (IRadioServiceListener listener : _serviceListeners)
                listener.seekStart();
        }
        // Seek Autostart
        else if (type.equals("seek_start_auto"))
        {
            _autoSeeking = true;
            _autoSeekInfo.autoSeekStaarted();
            for (IRadioServiceListener listener : _serviceListeners)
                listener.autoseekStart();
        }
        // Seek Found
        else if (type.equals("seek_found"))
        {

            int strength = bundle.getInt("strength", Settings.getAutoScanThreshold());
            _scanStatus.seekFound(_frequency, strength);
            for (IRadioServiceListener listener : _serviceListeners)
                listener.seekFound();
        }
        // Seek auto found
        else if (type.equals("seek_found_auto"))
        {
            _autoSeekInfo.seekFound();
            for (IRadioServiceListener listener : _serviceListeners)
                listener.autoseekFound();
        }
        // Seek end
        else if (type.equals("seek_end"))
        {
            _seeking = false;
            RdsRepository.RdsData data = _rdsRepo.getDataByFreq(_frequency);
            if (data.title != null && data.title.isEmpty()) {
                setTitle(data.title);
            }
            if (data.name != null && data.name.isEmpty()) {
                setPsn(data.name);
            }
            for (IRadioServiceListener listener : _serviceListeners)
                listener.seekEnd();
        }
        // Seek auto end
        else if (type.equals("seek_end_auto"))
        {
            _autoSeekInfo.autoSeekFinished();
            _autoSeeking = false;
            RdsRepository.RdsData data = _rdsRepo.getDataByFreq(_frequency);
            if (data.title != null && data.title.isEmpty()) {
                setTitle(data.title);
            }
            if (data.name != null && data.name.isEmpty()) {
                setPsn(data.name);
            }
            for (IRadioServiceListener listener : _serviceListeners)
                listener.autoseekEnd(_autoSeekInfo._autoSeekBouquet);
        }
    }

    public void skipTa() {
        if (_ta && !_continueAfterTa) {
            setRadioOn(false);
            setTaEnabled(false);
            _skipTa = true;
            _handler.sendEmptyMessageDelayed(MSG_TA_SKIP, _taSkipTime);
        }
    }
}

//TODO: "cfg_rds=" != 0 : RDS-Fähig

