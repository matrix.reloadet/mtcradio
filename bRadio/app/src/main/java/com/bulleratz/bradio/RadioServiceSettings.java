package com.bulleratz.bradio;

import android.content.Context;
import android.content.SharedPreferences;

public class RadioServiceSettings {
    SharedPreferences _sharedPrefs = null;
    SharedPreferences.Editor _editor = null;

    public RadioServiceSettings(Context context) {
        if (context != null) {
            _sharedPrefs = context.getSharedPreferences(R.string.app_id + "." + R.string.app_name, Context.MODE_PRIVATE);
            _editor = _sharedPrefs.edit();
        }
    }

    private static final String KEY_USE_MEDIA_KEYS = "USE_MEDIA_KEYS";
    private static final String KEY_USE_MICRONTEK_KEYS = "USE_MICRONTEK_KEYS";

    public boolean getUseMediaKeys() {
        return _sharedPrefs.getBoolean(KEY_USE_MEDIA_KEYS, true);
    }

    public void setUseMediaKeys(boolean useMediaKeys) {
        _editor.putBoolean(KEY_USE_MEDIA_KEYS, useMediaKeys).apply();
    }

    public boolean getUseMicrontekKeys() {
        return _sharedPrefs.getBoolean(KEY_USE_MICRONTEK_KEYS, true);
    }

    public void setUseMicrontekKeys(boolean useMicrontekKeys) {
        _editor.putBoolean(KEY_USE_MICRONTEK_KEYS, useMicrontekKeys).apply();
    }
}
