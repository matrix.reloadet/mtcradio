package com.bulleratz.bradio;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by maucher on 18.09.2017.
 */

public class StationsFragment extends Fragment {
    private int _buttonIndex = 0;
    private int _buttonCount = 1;

    private static final int MAX_BUTTON_COUNT = 5;

    private RadioStationButton _button0 = null;
    private RadioStationButton _button1 = null;
    private RadioStationButton _button2 = null;
    private RadioStationButton _button3 = null;
    private RadioStationButton _button4 = null;

    private RadioService _service = null;

    private ServiceConnection mConnection = new ServiceConnection()
    {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            _service = ((RadioServiceBinder)iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            _service = null;
        }
    };


    private static final String BUTTON_INDEX_ARG = "BUTTON_INDEX";

    private BouquetManager.IBouquetManagerListener _bouquetManagerListener = new BouquetManager.IBouquetManagerListener() {
        @Override
        public void selectedBouquetChanged(RadioStationBouquet bouquet) {
            //TODO:...
        }

        @Override
        public void selectedStationChanged(RadioStation station, int stationIndex) {
            updateButtons();
        }

        @Override
        public void bouquetAdded(RadioStationBouquet bouquet) {

        }

        @Override
        public void bouquetRemoved(RadioStationBouquet bouquet) {

        }
    };

    private RadioStationButton[] _buttons = new RadioStationButton[5];

    public int getButtonIndex() {
        return _buttonIndex;
    }

    public void setButtonIndex(int index)
    {
        if (_buttonIndex != index) {
            _buttonIndex = index;
            Bundle bundle = new Bundle();
            bundle.putInt(BUTTON_INDEX_ARG, _buttonIndex);
            setArguments(bundle);
        }
        updateButtons();
    }

    public int getButtonCount() {
        return _buttonCount;
    }

    private int getButtonIndex(View button)
    {
        if (button == _button0)
            return _buttonIndex;
        if (button == _button1)
            return _buttonIndex + 1;
        if (button == _button2)
            return _buttonIndex + 2;
        if (button == _button3)
            return _buttonIndex + 3;
        if (button == _button4)
            return _buttonIndex + 4;
        return -1;
    }

    private RadioStationButton getButton(int index)
    {
        index = index - _buttonIndex;
        if (index == 0)
            return _button0;
        if (index == 1)
            return _button1;
        if (index == 2)
            return _button2;
        if (index == 3)
            return _button3;
        if (index == 4)
            return _button4;
        return null;
    }

    public void showFrequencyDialog(final RadioStation station, int index) {
        final List<Integer> freqs = station.getAlternativeFrequencies();
        if (freqs.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
            builder.setTitle(station.getDisplayName() + " Frequencies");
            //builder.setIcon(R.drawable.station_default);
            final String[] items = new String[freqs.size() + 1];
            for (int i=0; i<freqs.size(); i++) {
                double freq = freqs.get(i).doubleValue() / 1000000;
                items[i] = (Double.toString(freq) + " Mhz");
            }
            items[items.length - 1] = "Remove All";


            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i <items.length - 1) {
                        if (_service != null) {
                            _service.setFrequency(freqs.get(i));
                        }
                    }
                    if ( i == items.length - 1) {
                        station.clearAlternatives();
                        RadioService.getBouquetManager().saveBouquets();
                    }
                }
            });
            builder.show();

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        Bundle arguments = getArguments();
        if (arguments != null) {
            _buttonIndex = arguments.getInt(BUTTON_INDEX_ARG, 0);
        }

        View.OnClickListener listener = new View.OnClickListener() {
            int clickCount = 0;
            @Override
            public void onClick(View view) {
                int index = getButtonIndex(view);
                if (index != -1) {
                    clickCount++;
                    if (clickCount == 1) {
                        Message msg = handler.obtainMessage();
                        msg.obj = view;
                        RadioService.getBouquetManager().selectStation(index);
                        handler.sendMessageDelayed(msg, 300);
                    }
                    if (clickCount == 2) {
                        clickCount = 0;
                        showFrequencyDialog(RadioService.getBouquetManager().getStation(index), index);
                    }
                }
            }
            Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    clickCount = 0;
                }
            };
        };

        View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int index = getButtonIndex(view);
                if (index != -1) {
                    RadioStation station = RadioService.getBouquetManager().getStation(index);
                    RadioStationEditorDialog dialog = new RadioStationEditorDialog(getActivity(), station, index);
                    dialog.show();
                    return true;
                }
                return false;
            }
        };

        LinearLayout rootView = new LinearLayout(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);

        for (int i=0; i < MAX_BUTTON_COUNT; i++) {
            params.setMargins(5,0,5,0);
            params.weight = 1;
            RadioStationButton button = null;
            button = new RadioStationButtonFactory().createButton(getContext());
            button.setLongClickable(true);
            button.setOnClickListener(listener);
            button.setOnLongClickListener(longClickListener);
            if (i >= getButtonCount()) {
                button.setVisibility(View.INVISIBLE);
            }
            rootView.addView(button, params);
            switch (i) {
                case 0:
                    _button0 = button;
                    break;
                case 1:
                    _button1 = button;
                    break;
                case 2:
                    _button2 = button;
                    break;
                case 3:
                    _button3 = button;
                    break;
                case 4:
                    _button4 = button;
                    break;
            }
        }

        _buttons[0] = _button0;
        _buttons[1] = _button1;
        _buttons[2] = _button2;
        _buttons[3] = _button3;
        _buttons[4] = _button4;

        RadioService.getBouquetManager().addBouquetManagerListener(_bouquetManagerListener);
        updateButtons();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentActivity activity = getActivity();
        if (activity != null && _service == null) {
                Intent intent = new Intent(activity, RadioService.class);
                activity.bindService(intent, mConnection, BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDestroy() {
        /* TODO: Called more than once ? */
        RadioService.getBouquetManager().removeBouquetManagerListener(_bouquetManagerListener);
        unregisterService();
        super.onDestroy();
    }

    private synchronized  void unregisterService() {
        FragmentActivity activity = getActivity();
        if (activity != null && _service != null) {
            _service = null;
            activity.unbindService(mConnection);
        }
    }

    public void updateButton(int index) {
        RadioStationBouquet _bouquet = RadioService.getBouquetManager()._bouquet;
        if (_bouquet != null) {
            for (int i=0; i<_buttons.length; i++) {
                if (_buttons[i] != null && (_buttonIndex + i) == index) {
                    RadioStation station = _bouquet.getStation(_buttonIndex + i);
                    _buttons[i].setRadioStation(station);
                    _buttons[i].setSelected(RadioService.getBouquetManager().getSelectedStation() == station);
                    if (station == null)
                        _buttons[i].setVisibility(View.INVISIBLE);
                    else
                        _buttons[i].setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void updateButtons() {
        RadioStationBouquet _bouquet = RadioService.getBouquetManager()._bouquet;
        if (_bouquet != null) {
            for (int i=_buttonIndex; i< (_buttonIndex + _buttons.length); i++) {
                updateButton(i);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateButtons();
    }

    public void setButtonCount(int count)
    {
        _buttonCount = count;
    }
}
