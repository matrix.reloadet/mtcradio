package com.bulleratz.bradio;

/**
 * Created by maucher on 25.09.2017.
 */

public interface IRadioStationListener {
    void radioStationChanged(RadioStation sender);
}
