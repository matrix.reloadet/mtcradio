package greenmesh.settings;

import android.os.Build;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public abstract class GenericListSettingsHandler<T> extends SettingsHandler {
    private ArrayList<T> _values = new ArrayList<>();

    public GenericListSettingsHandler(String name) {
        super(name);
    }

    public int getCount() {
        return _values.size();
    }

    public T getValue(int index) {
        if (index < 0 || index >= _values.size()) {
            return null;
        }
        return _values.get(index);
    }

    public void add(T value) {
        _values.add(value);
        notifyValueChanged();
    }

    public void setValue(int index, T value) {
        if (index < 0) {
            _values.add(0, value);
        }
        else if (index >= _values.size()) {
            _values.add(value);
        }
        else {
            if (_values.get(index).equals(value))
                return;
            _values.set(index, value);
        }
        notifyValueChanged();
    }

    public void moveDown(int index) {
        if (index < 1 || index >= _values.size())
            return;
        T value = _values.get(index);
        _values.remove(index);
        _values.add(index - 1, value);
        notifyValueChanged();
    }

    public void moveUp(int index) {
        if (index < 0 || index >= _values.size() - 1)
            return;
        T value = _values.get(index);
        _values.remove(index);
        _values.add(index + 1, value);
        notifyValueChanged();
    }

    protected abstract JSONObject toJsonObject(int index) throws JSONException;
    protected abstract void addJsonObject(JSONObject object, int index) throws JSONException;

    @Override
    public Object getValue() {
        return getValues();
    }

    @Override
    public void toJsonObject(JSONObject object) throws JSONException {
        JSONArray array = new JSONArray();
        for (int i=0; i< _values.size(); i++) {
            array.put(toJsonObject(i));
        }
        object.put("values", array);
    }

    public List<T> getValues() {
        ArrayList<T> rt = new ArrayList<>();
        for (int i=0; i< _values.size(); i++) {
            rt.add(_values.get(i));
        }
        return rt;
    }

    @Override
    public boolean setValue(Object value) {
        return false;
    }

    @Override
    protected boolean acceptValue(Object value) {
        return false;
    }

    @Override
    protected Object loadValue(JSONObject source) throws JSONException {
        JSONArray array = source.getJSONArray("values");
        if (array != null) {
            _values.clear();
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                addJsonObject(obj, i);
            }
        }
        onLoaded();
        return getValue();
    }

    public abstract void onLoaded();

    public boolean contains(T value) {
        if (value != null) {
            for (int i=0; i<_values.size(); i++) {
                if (_values.get(i) != null && _values.get(i).equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }
}
